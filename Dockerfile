FROM maven:3.8.5-eclipse-temurin-17-alpine
WORKDIR /app
COPY . .
CMD mvn package
EXPOSE 8080
CMD ["mvn", "spring-boot:run"]